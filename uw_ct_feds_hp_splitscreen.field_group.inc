<?php

/**
 * @file
 * uw_ct_feds_hp_splitscreen.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_hp_splitscreen_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_split_screen_content|node|feds_homepage_split_screen|form';
  $field_group->group_name = 'group_split_screen_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_split_screen';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '14',
    'children' => array(
      0 => 'body',
      1 => 'field_split_screen_subtitle',
      2 => 'field_heading',
      3 => 'field_split_screen_text_colour',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-split-screen-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_split_screen_content|node|feds_homepage_split_screen|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_split_screen_cta|node|feds_homepage_split_screen|form';
  $field_group->group_name = 'group_split_screen_cta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_split_screen';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Call to Action (CTA)',
    'weight' => '18',
    'children' => array(
      0 => 'field_split_screen_show_cta',
      1 => 'field_split_screen_cta_btn_text',
      2 => 'field_split_screen_cta_link',
      3 => 'field_split_screen_cta_colour',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-split-screen-cta field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_split_screen_cta|node|feds_homepage_split_screen|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Call to Action (CTA)');
  t('Content');

  return $field_groups;
}
