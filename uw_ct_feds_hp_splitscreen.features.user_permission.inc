<?php

/**
 * @file
 * uw_ct_feds_hp_splitscreen.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_splitscreen_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_homepage_split_screen content'.
  $permissions['create feds_homepage_split_screen content'] = array(
    'name' => 'create feds_homepage_split_screen content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_homepage_split_screen content'.
  $permissions['delete any feds_homepage_split_screen content'] = array(
    'name' => 'delete any feds_homepage_split_screen content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_homepage_split_screen content'.
  $permissions['delete own feds_homepage_split_screen content'] = array(
    'name' => 'delete own feds_homepage_split_screen content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_homepage_split_screen content'.
  $permissions['edit any feds_homepage_split_screen content'] = array(
    'name' => 'edit any feds_homepage_split_screen content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_homepage_split_screen content'.
  $permissions['edit own feds_homepage_split_screen content'] = array(
    'name' => 'edit own feds_homepage_split_screen content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_homepage_split_screen revision log entry'.
  $permissions['enter feds_homepage_split_screen revision log entry'] = array(
    'name' => 'enter feds_homepage_split_screen revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen authored by option'.
  $permissions['override feds_homepage_split_screen authored by option'] = array(
    'name' => 'override feds_homepage_split_screen authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen authored on option'.
  $permissions['override feds_homepage_split_screen authored on option'] = array(
    'name' => 'override feds_homepage_split_screen authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen comment setting option'.
  $permissions['override feds_homepage_split_screen comment setting option'] = array(
    'name' => 'override feds_homepage_split_screen comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen promote to front page option'.
  $permissions['override feds_homepage_split_screen promote to front page option'] = array(
    'name' => 'override feds_homepage_split_screen promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen published option'.
  $permissions['override feds_homepage_split_screen published option'] = array(
    'name' => 'override feds_homepage_split_screen published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen revision option'.
  $permissions['override feds_homepage_split_screen revision option'] = array(
    'name' => 'override feds_homepage_split_screen revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_split_screen sticky option'.
  $permissions['override feds_homepage_split_screen sticky option'] = array(
    'name' => 'override feds_homepage_split_screen sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_homepage_split_screen content'.
  $permissions['search feds_homepage_split_screen content'] = array(
    'name' => 'search feds_homepage_split_screen content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
