<?php

/**
 * @file
 * uw_ct_feds_hp_splitscreen.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_hp_splitscreen_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_homepage_split_screen';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds homepage split screen';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds homepage split screen';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Primary Image */
  $handler->display->display_options['fields']['field_split_screen_primary_image']['id'] = 'field_split_screen_primary_image';
  $handler->display->display_options['fields']['field_split_screen_primary_image']['table'] = 'field_data_field_split_screen_primary_image';
  $handler->display->display_options['fields']['field_split_screen_primary_image']['field'] = 'field_split_screen_primary_image';
  $handler->display->display_options['fields']['field_split_screen_primary_image']['label'] = '';
  $handler->display->display_options['fields']['field_split_screen_primary_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_split_screen_primary_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_split_screen_primary_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_split_screen_primary_image']['type'] = 'list_key';
  /* Field: Content: Left Image */
  $handler->display->display_options['fields']['field_left_image']['id'] = 'field_left_image';
  $handler->display->display_options['fields']['field_left_image']['table'] = 'field_data_field_left_image';
  $handler->display->display_options['fields']['field_left_image']['field'] = 'field_left_image';
  $handler->display->display_options['fields']['field_left_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_left_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_left_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Left background colour */
  $handler->display->display_options['fields']['field_left_background_colour']['id'] = 'field_left_background_colour';
  $handler->display->display_options['fields']['field_left_background_colour']['table'] = 'field_data_field_left_background_colour';
  $handler->display->display_options['fields']['field_left_background_colour']['field'] = 'field_left_background_colour';
  $handler->display->display_options['fields']['field_left_background_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_left_background_colour']['element_default_classes'] = FALSE;
  /* Field: Content: Right Image */
  $handler->display->display_options['fields']['field_right_image']['id'] = 'field_right_image';
  $handler->display->display_options['fields']['field_right_image']['table'] = 'field_data_field_right_image';
  $handler->display->display_options['fields']['field_right_image']['field'] = 'field_right_image';
  $handler->display->display_options['fields']['field_right_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_right_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_right_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_right_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Right background colour */
  $handler->display->display_options['fields']['field_right_background_colour']['id'] = 'field_right_background_colour';
  $handler->display->display_options['fields']['field_right_background_colour']['table'] = 'field_data_field_right_background_colour';
  $handler->display->display_options['fields']['field_right_background_colour']['field'] = 'field_right_background_colour';
  $handler->display->display_options['fields']['field_right_background_colour']['exclude'] = TRUE;
  /* Field: Content: Subtitle */
  $handler->display->display_options['fields']['field_split_screen_subtitle']['id'] = 'field_split_screen_subtitle';
  $handler->display->display_options['fields']['field_split_screen_subtitle']['table'] = 'field_data_field_split_screen_subtitle';
  $handler->display->display_options['fields']['field_split_screen_subtitle']['field'] = 'field_split_screen_subtitle';
  $handler->display->display_options['fields']['field_split_screen_subtitle']['exclude'] = TRUE;
  /* Field: Content: Heading */
  $handler->display->display_options['fields']['field_heading']['id'] = 'field_heading';
  $handler->display->display_options['fields']['field_heading']['table'] = 'field_data_field_heading';
  $handler->display->display_options['fields']['field_heading']['field'] = 'field_heading';
  $handler->display->display_options['fields']['field_heading']['exclude'] = TRUE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  /* Field: Content: Hero Content Text Colour */
  $handler->display->display_options['fields']['field_split_screen_text_colour']['id'] = 'field_split_screen_text_colour';
  $handler->display->display_options['fields']['field_split_screen_text_colour']['table'] = 'field_data_field_split_screen_text_colour';
  $handler->display->display_options['fields']['field_split_screen_text_colour']['field'] = 'field_split_screen_text_colour';
  $handler->display->display_options['fields']['field_split_screen_text_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_split_screen_text_colour']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_split_screen_text_colour']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_split_screen_text_colour']['type'] = 'services';
  $handler->display->display_options['fields']['field_split_screen_text_colour']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Field: Content: Show CTA */
  $handler->display->display_options['fields']['field_split_screen_show_cta']['id'] = 'field_split_screen_show_cta';
  $handler->display->display_options['fields']['field_split_screen_show_cta']['table'] = 'field_data_field_split_screen_show_cta';
  $handler->display->display_options['fields']['field_split_screen_show_cta']['field'] = 'field_split_screen_show_cta';
  $handler->display->display_options['fields']['field_split_screen_show_cta']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_split_screen_show_cta']['type'] = 'list_key';
  /* Field: Content: Button Text */
  $handler->display->display_options['fields']['field_split_screen_cta_btn_text']['id'] = 'field_split_screen_cta_btn_text';
  $handler->display->display_options['fields']['field_split_screen_cta_btn_text']['table'] = 'field_data_field_split_screen_cta_btn_text';
  $handler->display->display_options['fields']['field_split_screen_cta_btn_text']['field'] = 'field_split_screen_cta_btn_text';
  $handler->display->display_options['fields']['field_split_screen_cta_btn_text']['exclude'] = TRUE;
  /* Field: Content: CTA Colour Selection */
  $handler->display->display_options['fields']['field_split_screen_cta_colour']['id'] = 'field_split_screen_cta_colour';
  $handler->display->display_options['fields']['field_split_screen_cta_colour']['table'] = 'field_data_field_split_screen_cta_colour';
  $handler->display->display_options['fields']['field_split_screen_cta_colour']['field'] = 'field_split_screen_cta_colour';
  $handler->display->display_options['fields']['field_split_screen_cta_colour']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_split_screen_cta_colour']['type'] = 'list_key';
  /* Field: Content: CTA Link */
  $handler->display->display_options['fields']['field_split_screen_cta_link']['id'] = 'field_split_screen_cta_link';
  $handler->display->display_options['fields']['field_split_screen_cta_link']['table'] = 'field_data_field_split_screen_cta_link';
  $handler->display->display_options['fields']['field_split_screen_cta_link']['field'] = 'field_split_screen_cta_link';
  $handler->display->display_options['fields']['field_split_screen_cta_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_split_screen_cta_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_split_screen_cta_link']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_split_screen_cta_link']['settings'] = array(
    'custom_title' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body_1']['id'] = 'body_1';
  $handler->display->display_options['fields']['body_1']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body_1']['field'] = 'body';
  $handler->display->display_options['fields']['body_1']['label'] = '';
  $handler->display->display_options['fields']['body_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body_1']['alter']['text'] = '<div class="split-screen-container">
  <div class="split-screen prime-img-[field_split_screen_primary_image]">
    <div class="bg-left">[field_left_image][field_left_background_colour]</div>
    <div class="bg-right">[field_right_image][field_right_background_colour]</div>
    <div class="split-screen-content txt-color-[field_split_screen_text_colour]">
      <sub>[field_split_screen_subtitle]</sub>
      [field_heading]
      [body]
      <div class="hero-cta cta-theme-[field_split_screen_cta_colour] cta-visibility-[field_split_screen_show_cta]">
        <a href="[field_split_screen_cta_link]">[field_split_screen_cta_btn_text]</a>
      </div>
    </div>
  </div>
</div>';
  $handler->display->display_options['fields']['body_1']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_homepage_split_screen' => 'feds_homepage_split_screen',
  );

  /* Display: Hero Split Screen Block */
  $handler = $view->new_display('block', 'Hero Split Screen Block', 'block');
  $translatables['feds_homepage_split_screen'] = array(
    t('Master'),
    t('Feds homepage split screen'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Left Image'),
    t('Left background colour'),
    t('Right Image'),
    t('Right background colour'),
    t('Subtitle'),
    t('Heading'),
    t('Body'),
    t('Hero Content Text Colour'),
    t('Show CTA'),
    t('Button Text'),
    t('CTA Colour Selection'),
    t('CTA Link'),
    t('<div class="split-screen-container">
  <div class="split-screen prime-img-[field_split_screen_primary_image]">
    <div class="bg-left">[field_left_image][field_left_background_colour]</div>
    <div class="bg-right">[field_right_image][field_right_background_colour]</div>
    <div class="split-screen-content txt-color-[field_split_screen_text_colour]">
      <sub>[field_split_screen_subtitle]</sub>
      [field_heading]
      [body]
      <div class="hero-cta cta-theme-[field_split_screen_cta_colour] cta-visibility-[field_split_screen_show_cta]">
        <a href="[field_split_screen_cta_link]">[field_split_screen_cta_btn_text]</a>
      </div>
    </div>
  </div>
</div>'),
    t('Hero Split Screen Block'),
  );
  $export['feds_homepage_split_screen'] = $view;

  return $export;
}
