<?php

/**
 * @file
 * uw_ct_feds_hp_splitscreen.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_hp_splitscreen_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_feds_hp_splitscreen_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_hp_splitscreen_node_info() {
  $items = array(
    'feds_homepage_split_screen' => array(
      'name' => t('Feds Homepage Split Screen'),
      'base' => 'node_content',
      'description' => t('Hero split screen image and content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
